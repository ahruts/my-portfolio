const gulp = require("gulp");
const clean = require("gulp-clean");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const minifyJS = require("gulp-uglify-es").default;
const htmlmin = require("gulp-htmlmin");
const imagemin = require("gulp-imagemin");
const concat = require("gulp-concat");

gulp.task("clean", function () {
  return gulp.src("public/*", { read: false }).pipe(clean());
});

gulp.task("build-html", function () {
  return gulp
    .src("src/index.html")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("public/"));
});

gulp.task("build-css", function () {
  return gulp
    .src("src/scss/style.scss")
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(concat("style.min.css"))
    .pipe(gulp.dest("public/"));
});

gulp.task("build-js", function () {
  return gulp
    .src("src/js/*.js")
    .pipe(minifyJS())
    .pipe(concat("script.min.js"))
    .pipe(gulp.dest("public/"));
});

gulp.task("minify-img", function () {
  return gulp.src("src/img/**/*").pipe(gulp.dest("public/img/"));
});

gulp.task(
  "build",
  gulp.series(
    "clean",
    gulp.parallel("build-html", "build-js", "build-css", "minify-img")
  )
);


  gulp.task("dev", function () {
    gulp.watch("src/*.html", gulp.series("build-html"));
    gulp.watch("src/scss/*.scss", gulp.series("build-css"));
    gulp.watch("src/js/*.js", gulp.series("build-js"));
  });